package Lesson7.Task.TableRelations.repository;

import Lesson7.Task.TableRelations.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BranchRepository extends JpaRepository<Branch, Long> {



//    @Query(value = "select * from branch where market_id=?1", nativeQuery = true)
//    List<Branch> getFromDbWithSql(Long id);
//
//    @Query(value = "select b from Branch b where b.market.id=:id ")
//    List<Branch> getFromDbWithHibernateQL(Long id);
//
//    @Query(value = "select b from Branch b join fetch b.address a where b.market.id=:id")
//    List<Branch> getFromDbWithJoin(Long id);
//
//    @Query(name = "findBranchesByMarketId")
//    List<Branch> getBranchesWithHibernateQuery(Long id);


}
