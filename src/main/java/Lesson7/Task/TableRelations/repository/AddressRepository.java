package Lesson7.Task.TableRelations.repository;

import Lesson7.Task.TableRelations.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Long> {
}
