package Lesson7.Task.TableRelations.repository;

import Lesson7.Task.TableRelations.entity.Market;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface MarketRepository extends JpaRepository<Market, Long> {

    @Query(value = "select m.id,m.name,m.type," +
            "b.id as branch_id,b.name as branch_name," +
            "b.count_of_employee,a.id as address_id,a.name as address_name," +
            "r.id as region_id,r.name as region_name " +
            "from market m " +
            "left join market_region mr on m.id=mr.market_id " +
            "left join region r on mr.region_id=r.id " +
            "left join branch b on m.id=b.market_id " +
            "left join address a on b.address_id=a.id ", nativeQuery = true)
    List<Market> findAllBy();

//    @EntityGraph(value = "GetMarketsWithNamedGraph")
//    List<Market> findAllBy();

    @Override
    @EntityGraph(attributePaths = {"branches", "regions", "branches.address"})
    List<Market> findAll();

    @Query(value = "select m from Market m join fetch m.branches b join fetch b.address a join fetch m.regions")
    List<Market> getAllWithJPQL();

    @Query(value = "select m from Market m join fetch m.regions r where r.id=:id")
    List<Market> findAllByRegionId(Long id);


}
