package Lesson7.Task.TableRelations.repository;

import Lesson7.Task.TableRelations.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegionRepository extends JpaRepository<Region,Long> {

}
