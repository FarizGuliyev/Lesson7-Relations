package Lesson7.Task.TableRelations.dto;

import Lesson7.Task.TableRelations.entity.Address;
import Lesson7.Task.TableRelations.entity.Market;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchResponse {
    int id;
    String name;
    Integer countOfEmployee;
    private Address address;
}
