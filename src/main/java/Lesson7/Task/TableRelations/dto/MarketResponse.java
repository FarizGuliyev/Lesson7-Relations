package Lesson7.Task.TableRelations.dto;

import Lesson7.Task.TableRelations.entity.Branch;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketResponse {
    int id;
    String name;
    String type;
    @Builder.Default
    List<BranchResponse> branches=new ArrayList<>();
    @Builder.Default
    List<RegionResponse> regions=new ArrayList<>();
}
