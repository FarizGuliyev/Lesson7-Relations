package Lesson7.Task.TableRelations.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import jakarta.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "branch")
//@NamedQueries({
//        @NamedQuery(name = "findBranchesByMarketId", query = "select b from Branch b where b.market.id=:id")
//})
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    Integer countOfEmployee;
    @JsonIgnore
    @ManyToOne
    @ToString.Exclude
    Market market;


    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Address address;
}
