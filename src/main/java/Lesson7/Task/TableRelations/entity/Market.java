package Lesson7.Task.TableRelations.entity;

import jakarta.persistence.*;

import lombok.*;
import lombok.experimental.FieldDefaults;

import jakarta.validation.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "market")
@NamedEntityGraph(name = "GetMarketsWithNamedGraph",
        attributeNodes = {
                @NamedAttributeNode(value = "branches", subgraph = "branchWithAddress"),
                @NamedAttributeNode(value = "regions")
        },
        subgraphs = {@NamedSubgraph(name = "branchWithAddress", attributeNodes = @NamedAttributeNode("address")),}
)
public class Market {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @NotEmpty
    String name;

    String type;

    @OneToMany(mappedBy = "market", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Builder.Default
    Set<Branch> branches = new HashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @Builder.Default
    @JoinTable(name = "market_region",
            joinColumns = @JoinColumn(name = "market_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "region_id", referencedColumnName = "id"))
    Set<Region> regions = new HashSet<>();

}
