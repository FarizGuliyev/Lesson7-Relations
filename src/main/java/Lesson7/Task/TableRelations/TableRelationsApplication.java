package Lesson7.Task.TableRelations;

import Lesson7.Task.TableRelations.entity.Address;
import Lesson7.Task.TableRelations.entity.Branch;
import Lesson7.Task.TableRelations.entity.Market;
import Lesson7.Task.TableRelations.repository.AddressRepository;
import Lesson7.Task.TableRelations.repository.BranchRepository;
import Lesson7.Task.TableRelations.repository.MarketRepository;
import Lesson7.Task.TableRelations.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class TableRelationsApplication implements CommandLineRunner {

    private final MarketRepository marketRepository;
    private final BranchRepository branchRepository;
    private final AddressRepository addressRepository;
    private final RegionRepository regionRepository;
    private final EntityManagerFactory entityManagerFactory;

    public static void main(String[] args) {
        SpringApplication.run(TableRelationsApplication.class, args);
    }

    @Override
//    @Transactional
    public void run(String... args) throws Exception {

        marketRepository.findAllBy().forEach(System.out::println);


//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        List<Branch> branches = entityManager.createQuery("select b from Branch b join Address a where b.market.id=:id", Branch.class)
//                .setParameter("id", 1)
//                .getResultList();
//
//        List<Branch> resultList = entityManager.createNamedQuery("findBranchesByMarketId", Branch.class)
//                .setParameter("id", 1)
//                .getResultList();

//        JPAStreamer jpaStreamer=JPAStreamer.of(entityManagerFactory);
//        jpaStreamer.stream(Branch.class)
//                .forEach(branch -> System.out.println(branch));


//        List<Branch> allBranchesWithHibernateQuery=branchRepository.getBranchesWithHibernateQuery(1L);

//        List<Branch> all = branchRepository.getFromDbWithSql(1L);

//        List<Branch> allFromHibernate = branchRepository.getFromDbWithHibernateQL(1L);

//        List<Branch> allWithJoin=branchRepository.getFromDbWithJoin(1L);

//        allBranchesWithHibernateQuery.forEach(System.out::println);

//        //		_____________________
//		Market bravoMarket = Market.builder()
//				.name("Bravo")
//				.type("Hyper")
//				.build();
//		Market arazMarket = Market.builder()
//				.name("Araz")
//				.type("Super")
//				.build();
//
//		marketRepository.save(bravoMarket);
//		marketRepository.save(arazMarket);
//
////		_________________________
//		Address address1 = Address.builder()
//				.name("Huseyn Cavid 145 A")
//				.build();
//
//		Address address2 = Address.builder()
//				.name("Ahmad Racabli 22 B")
//				.build();
//
//		//      _____________________________
//		Branch koroghluBranch = Branch.builder()
//				.name("koroghluBravo")
//				.market(bravoMarket)
//				.address(address1)
//				.build();
//
//		Branch nerimanovBranch=Branch.builder()
//				.name("nerimanovBravo")
//				.market(bravoMarket)
//				.address(address2)
//				.build();
//
//		bravoMarket.getBranches().add(koroghluBranch);
//		bravoMarket.getBranches().add(nerimanovBranch);
//
//		addressRepository.save(address1);
//		addressRepository.save(address2);
//
//		branchRepository.save(koroghluBranch);
//		branchRepository.save(nerimanovBranch);
//
//        marketRepository.findAll().forEach(System.out::println);
    }
}
