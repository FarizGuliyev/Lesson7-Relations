package Lesson7.Task.TableRelations.service;

import Lesson7.Task.TableRelations.dto.AddressRequest;
import Lesson7.Task.TableRelations.dto.AddressResponse;
import Lesson7.Task.TableRelations.entity.Address;
import Lesson7.Task.TableRelations.entity.Branch;
import Lesson7.Task.TableRelations.repository.AddressRepository;
import Lesson7.Task.TableRelations.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;

    public AddressResponse create(Long branchId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        Address address = modelMapper.map(request, Address.class);
        branch.setAddress(address);
        address = addressRepository.save(address);
        branchRepository.save(branch);
        return modelMapper.map(address, AddressResponse.class);
    }

    @Transactional
    public AddressResponse get(Long addressId) {
        Address address = addressRepository.findById(addressId).orElseThrow(() ->
                new RuntimeException(String.format("Address with id %s not found ", addressId)));
        return modelMapper.map(address, AddressResponse.class);
    }


    public List<AddressResponse> getAll() {
        List<Address> addresses = addressRepository.findAll();
        List<AddressResponse> addressResponses = new ArrayList<>();
        addresses.forEach(address ->
                addressResponses.add(modelMapper.map(address, AddressResponse.class)));
        return addressResponses;
    }

    @Transactional
    public AddressResponse update(Long addressId, Long branchId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));

        Address address = addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException(String.format("Address with id %s not found", addressId)));

        address.setName(request.getName());

        addressRepository.save(address);
        return modelMapper.map(address, AddressResponse.class);
    }

    @Transactional
    public void delete(Long branchId, Long addressId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        branch.setAddress(null);
        branchRepository.save(branch);
        addressRepository.deleteById(addressId);
    }
}
