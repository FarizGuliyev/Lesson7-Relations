package Lesson7.Task.TableRelations.service;

import Lesson7.Task.TableRelations.dto.MarketRequest;
import Lesson7.Task.TableRelations.dto.MarketResponse;
import Lesson7.Task.TableRelations.entity.Market;
import Lesson7.Task.TableRelations.entity.Region;
import Lesson7.Task.TableRelations.repository.MarketRepository;
import Lesson7.Task.TableRelations.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MarketService {

    private final MarketRepository marketRepository;
    private final RegionRepository regionRepository;
    private final ModelMapper modelMapper;

    public MarketResponse create(MarketRequest request) {
        Market market = modelMapper.map(request, Market.class);
        market = marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }

    @Transactional
    public MarketResponse get(Long marketId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        return modelMapper.map(market, MarketResponse.class);
    }

    public List<MarketResponse> getAll() {
        List<Market> markets = marketRepository.findAll();
        List<MarketResponse> marketResponses = new ArrayList<>();
        markets.forEach(market ->
                marketResponses.add(modelMapper.map(market, MarketResponse.class)));
        return marketResponses;
    }

    @Transactional
    public MarketResponse update(Long marketId, MarketRequest marketRequest) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        market.setName(marketRequest.getName());
        market.setType(marketRequest.getType());
        marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }

    public MarketResponse updateMarketWithRegion(Long marketId, Long regionId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));

        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException(String.format("Region with id %s not found", regionId)));

        market.getRegions().add(region);
        market = marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }


    @Transactional
    public void delete(Long marketId) {
        marketRepository.deleteById(marketId);
    }


}
