package Lesson7.Task.TableRelations.service;

import Lesson7.Task.TableRelations.dto.BranchRequest;
import Lesson7.Task.TableRelations.dto.BranchResponse;
import Lesson7.Task.TableRelations.dto.MarketResponse;
import Lesson7.Task.TableRelations.entity.Branch;
import Lesson7.Task.TableRelations.entity.Market;
import Lesson7.Task.TableRelations.repository.BranchRepository;
import Lesson7.Task.TableRelations.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;


    public BranchResponse create(Long marketId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        Branch branch = modelMapper.map(request, Branch.class);
        branch.setMarket(market);
        market.getBranches().add(branch);
        branch = branchRepository.save(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }

    @Transactional
    public BranchResponse get(Long branchId) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(() ->
                new RuntimeException(String.format("Branch with id %s not found ", branchId)));
        return modelMapper.map(branch, BranchResponse.class);
    }

    public List<BranchResponse> getAll() {
        List<Branch> branches = branchRepository.findAll();
        List<BranchResponse> branchResponses = new ArrayList<>();
        branches.forEach(branch ->
                branchResponses.add(modelMapper.map(branch, BranchResponse.class)));
        return branchResponses;
    }

    @Transactional
    public BranchResponse update(Long marketId, Long branchId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));

        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        branch.setName(request.getName());
        branch.setCountOfEmployee(request.getCountOfEmployee());
        branchRepository.save(branch);
        return modelMapper.map(branch, BranchResponse.class);
    }

    @Transactional
    public void delete(Long branchId) {
        branchRepository.deleteById(branchId);

    }
}
