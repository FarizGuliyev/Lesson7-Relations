package Lesson7.Task.TableRelations.service;

import Lesson7.Task.TableRelations.dto.BranchResponse;
import Lesson7.Task.TableRelations.dto.RegionRequest;
import Lesson7.Task.TableRelations.dto.RegionResponse;
import Lesson7.Task.TableRelations.entity.Branch;
import Lesson7.Task.TableRelations.entity.Market;
import Lesson7.Task.TableRelations.entity.Region;
import Lesson7.Task.TableRelations.repository.MarketRepository;
import Lesson7.Task.TableRelations.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;

    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;


    public RegionResponse create(RegionRequest request) {
        Region region = modelMapper.map(request, Region.class);
        region = regionRepository.save(region);
        return modelMapper.map(region, RegionResponse.class);
    }

    @Transactional
    public RegionResponse get(Long regionId) {
        Region region = regionRepository.findById(regionId).get();
        return modelMapper.map(region, RegionResponse.class);
    }

    public List<RegionResponse> getAll() {
        List<Region> regions = regionRepository.findAll();
        List<RegionResponse> regionResponses = new ArrayList<>();
        regions.forEach(region ->
                regionResponses.add(modelMapper.map(region, RegionResponse.class)));
        return regionResponses;
    }

    public RegionResponse update(Long regionId, RegionRequest regionRequest) {
        Region region = regionRepository.findById(regionId).orElseThrow(RuntimeException::new);

        Region region1 = modelMapper.map(regionRequest, Region.class);
        region1.setId(region.getId());
        Region saveRegion = regionRepository.save(region1);
        return modelMapper.map(saveRegion, RegionResponse.class);
    }

    public void delete(Long regionId) {
        List<Market> markets = marketRepository.findAllByRegionId(regionId);

        Region region = regionRepository.findById(regionId).orElseThrow(RuntimeException::new);
        markets.forEach(
                market -> market.getRegions().remove(region));

        marketRepository.saveAll(markets);

        regionRepository.delete(region);

    }
}
