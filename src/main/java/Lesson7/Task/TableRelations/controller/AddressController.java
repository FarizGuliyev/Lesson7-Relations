package Lesson7.Task.TableRelations.controller;

import Lesson7.Task.TableRelations.dto.AddressRequest;
import Lesson7.Task.TableRelations.dto.AddressResponse;
import Lesson7.Task.TableRelations.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @PostMapping("/{branchId}")
    public AddressResponse create(@PathVariable Long branchId, @RequestBody AddressRequest request) {
        return addressService.create(branchId, request);
    }

    @GetMapping("/address/{addressId}")
    public AddressResponse get(@PathVariable Long addressId) {
        return addressService.get(addressId);
    }

    @GetMapping("/addresses")
    public List<AddressResponse> getAll() {
        return addressService.getAll();
    }

    @PutMapping("/{branchId}/address/{addressId}")
    public AddressResponse update(@PathVariable Long branchId,
                                  @PathVariable Long addressId,
                                  @RequestBody AddressRequest request) {
        return addressService.update(addressId, branchId, request);
    }

    @DeleteMapping("/{branchId}/address/{addressId}")
    public void delete(@PathVariable Long branchId, @PathVariable Long addressId) {
        addressService.delete(branchId,addressId);
    }
}
