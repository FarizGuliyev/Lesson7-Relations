package Lesson7.Task.TableRelations.controller;

import Lesson7.Task.TableRelations.dto.BranchRequest;
import Lesson7.Task.TableRelations.dto.BranchResponse;
import Lesson7.Task.TableRelations.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class BranchController {
    private final BranchService branchService;

    @PostMapping("/{marketId}")
    public BranchResponse create(@PathVariable Long marketId, @RequestBody BranchRequest request) {
        return branchService.create(marketId, request);
    }

    @GetMapping("/branch/{branchId}")
    public BranchResponse get(@PathVariable Long branchId) {
        return branchService.get(branchId);
    }

    @GetMapping("/branches")
    public List<BranchResponse> getAll() {
        return branchService.getAll();
    }

    @PutMapping("/{marketId}/branch/{branchId}")
    public BranchResponse update(@PathVariable Long marketId,
                                 @PathVariable Long branchId,
                                 @RequestBody BranchRequest request) {
        return branchService.update(marketId, branchId, request);
    }

    @DeleteMapping("/branch/{branchId}")
    public void delete(@PathVariable Long branchId) {
        branchService.delete(branchId);
    }

}
