package Lesson7.Task.TableRelations.controller;

import Lesson7.Task.TableRelations.dto.MarketRequest;
import Lesson7.Task.TableRelations.dto.MarketResponse;
import Lesson7.Task.TableRelations.entity.Region;
import Lesson7.Task.TableRelations.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {
    private final MarketService marketService;

    @PostMapping
    public MarketResponse create(@RequestBody @Valid MarketRequest request) {
        return marketService.create(request);
    }

    @GetMapping("/{marketId}")
    public MarketResponse get(@PathVariable Long marketId) {
        return marketService.get(marketId);
    }

    @GetMapping
    public List<MarketResponse> getAll() {
        return marketService.getAll();
    }

    @PutMapping("/{marketId}")
    public MarketResponse update(@PathVariable Long marketId,@RequestBody @Valid MarketRequest marketRequest) {
        return marketService.update(marketId,marketRequest);
    }

    @PutMapping("/{marketId}/withRegion/{regionId}")
    public MarketResponse updateRegions(@PathVariable Long marketId,@PathVariable Long regionId) {
        return marketService.updateMarketWithRegion(marketId,regionId);
    }

    @DeleteMapping("/{marketId}")
    public void delete(@PathVariable Long marketId) {
        marketService.delete(marketId);
    }

}
