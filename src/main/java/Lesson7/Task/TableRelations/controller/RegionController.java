package Lesson7.Task.TableRelations.controller;

import Lesson7.Task.TableRelations.dto.MarketRequest;
import Lesson7.Task.TableRelations.dto.MarketResponse;
import Lesson7.Task.TableRelations.dto.RegionRequest;
import Lesson7.Task.TableRelations.dto.RegionResponse;
import Lesson7.Task.TableRelations.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {
    private final RegionService regionService;

    @PostMapping
    public RegionResponse create(@RequestBody RegionRequest request) {
        return regionService.create(request);
    }

    @GetMapping("/{regionId}")
    public RegionResponse get(@PathVariable Long regionId) {
        return regionService.get(regionId);
    }

    @GetMapping
    public List<RegionResponse> getAll() {
        return regionService.getAll();
    }

    @PutMapping("/{regionId}")
    public RegionResponse update(@PathVariable Long regionId, @RequestBody RegionRequest regionRequest) {
        return regionService.update(regionId, regionRequest);
    }

    @DeleteMapping("/{regionId}")
    public void delete(@PathVariable Long regionId) {
        regionService.delete(regionId);
    }

}
